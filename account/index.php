<?php

session_start();

if (!isset($_SESSION['email'])) {
    header("Location:login.php");
}

include '../Model.php';
include '../part/top.php';

$model = new Model();
$user = $model->getUser()->fetch_assoc();

?>


<!-- ****** Checkout Area Start ****** -->
<div class="checkout_area section_padding_100 shop">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6">
                <div class="checkout_details_area mt-50 clearfix">

                    <div class="cart-page-heading">
                        <h5>Setting</h5>
                    </div>

                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-12 mb-3">
                                <label for="first_name">Name <span>*</span></label>
                                <input type="text" class="form-control" id="first_name" name="name" value="<?= $user['name'] ?>" required>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="street_address">Address <span>*</span></label>
                                <input type="text" class="form-control mb-3" id="street_address" name="address" value="<?= $user['address'] ?>">
                            </div>
                            <div class="col-12 mb-3">
                                <label for="phone_number">Phone No <span>*</span></label>
                                <input type="number" class="form-control" id="phone_number" name="phone" value="<?= $user['no_hp'] ?>">
                            </div>
                            <div class="col-12 mb-4">
                                <label for="email_address">Email Address <span>*</span></label>
                                <input type="email" class="form-control" id="email_address" name="email" value="<?= $user['email'] ?>" disabled>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="oldPass">Old Password <span>*</span></label>
                                <input type="password" class="form-control" id="oldPass" name="oldPass">
                            </div>
                            <div class="col-12 mb-3">
                                <label for="newPass">New Password <span>*</span></label>
                                <input type="password" class="form-control" id="newPass" name="newPass">
                            </div>

                            <input type="submit" name="btnSubmit"  class="btn karl-checkout-btn" value="Save">
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- ****** Checkout Area End ****** -->


<?php

include '../part/bottom.php';

if (isset($_POST['btnSubmit'])) {
  $name = $_POST['name'];
  $address = $_POST['address'];
  $phone = $_POST['phone'];
  $user = $_SESSION['email'];
  $qPass = '';

  $conn = $model->conn();
  if ($_POST['oldPass'] && $_POST['newPass']) {
    $hashedPass = password_hash( $_POST['newPass'], PASSWORD_BCRYPT );
    $qPass .= ", password = '$hashedPass'";
  }

  $query = "UPDATE users SET name='$name', address='$address', no_hp='$phone' $qPass WHERE email='$user' ";
  $exec = $conn->query($query);
  if ($exec == true) {
      echo "<script>alert('Success change data'); window.location.href = '/account';</script>";
  }else{
      echo "<script>alert('Failed change data')</script>";
  }
}

?>
