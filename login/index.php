<?php

session_start();

include '../Model.php';
$model = new Model();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Online Tickets Order | Home</title>

    <!-- Favicon  -->
    <link rel="icon" href="/assets/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="/assets/css/core-style.css">
    <link rel="stylesheet" href="/assets/style.css">

    <!-- Responsive CSS -->
    <link href="/assets/css/responsive.css" rel="stylesheet">

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="/assets/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Bootstrap js -->
    <script src="/assets/js/bootstrap.min.js"></script>
</head>

<body>
    <div id="wrapper">

        <!-- ****** Header Area Start ****** -->
        <header class="header_area">
            <!-- Top Header Area Start -->
            <div class="top_header_area">
                <div class="container h-100">
                    <div class="row h-100 align-items-center justify-content-end">

                        <div class="col-12 col-lg-7">
                            <div class="top_single_area d-flex align-items-center">
                                <!-- Logo Area -->
                                <div class="top_logo">
                                    <a href="/"><img style="width:168px;height:50px;" src="/assets/img/core-img/logo1.png" alt=""></a>
                                </div>
                                <!-- Cart & Menu Area -->
                                <div class="header-cart-menu d-flex align-items-center ml-auto">
                                    <!-- Cart Area -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>
        <!-- ****** Header Area End ****** -->

<!-- ****** Checkout Area Start ****** -->
<div class="checkout_area section_padding_100 shop">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6">
                <div class="checkout_details_area mt-50 clearfix">

                    <div class="cart-page-heading">
                        <h5>Sign Up</h5>
                    </div>

                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-12 mb-4">
                                <label for="email_address">Email Address <span>*</span></label>
                                <input type="email" class="form-control" id="email_address" name="email">
                            </div>
                            <div class="col-12 mb-3">
                                <label for="password">Password <span>*</span></label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>

                            <input type="submit" name="btnLogin"  class="btn karl-checkout-btn" value="Sign In">
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- ****** Checkout Area End ****** -->


<?php

include '../part/bottom.php';

$conn = $model->conn();
if (isset($_POST['btnLogin'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $query = "SELECT * FROM users WHERE email='$email' ";
    $exec = $conn->query($query);

    if ($exec->num_rows > 0) {
        $user = $exec->fetch_assoc();
        if ( password_verify($password, $user['password']) ) {
            $_SESSION['email'] = $email;
            $_SESSION['status'] = "user";
            echo "<script>alert('Logged In');  window.location.href = '/';</script>";
        }else{
            echo "<script>alert('Wrong password')</script>";
        }
    }else{
          echo "<script>alert('User not exists')</script>";
    }
}
?>
