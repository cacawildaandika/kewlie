<?php

session_start();

if (!isset($_SESSION['email'])) {
    header("Location:login.php");
}

include '../Model.php';
include '../part/top.php';

$model = new Model();
$cat = '';
$products = $model->getAllProducts($cat);
if (isset($_GET['cat'])) {
  $products = $model->getAllProducts($_GET['cat']);
}

$categories = $model->getAllCategory();
?>

        <section class="shop_grid_area section_padding_100 shop">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-3">
                        <div class="shop_sidebar_area">

                            <div class="widget catagory mb-50">
                                <!--  Side Nav  -->
                                <div class="nav-side-menu">
                                    <h6 class="mb-0">Categories</h6>
                                    <div class="menu-list">
                                        <ul id="menu-content2" class="menu-content collapse out">
                                            <!-- Single Item -->
                                            <?php $n=1; while($item = $categories->fetch_assoc()) :?>
                                            <li><a href="<?= "/shop?cat=" . $item['id'] ?>"><?= $item['name'] ?></a></li>
                                          <?php endwhile ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-8 col-lg-9">
                        <div class="shop_grid_product_area">
                            <div class="row">

                                <?php $n=1; while($item = $products->fetch_assoc()) :?>
                                <!-- Single gallery Item -->
                                <div class="col-12 col-sm-6 col-lg-4 single_gallery_item wow fadeInUpBig" data-wow-delay="0.2s">
                                    <!-- Product Image -->
                                    <div class="product-img">
                                        <img style="width:255px;height:368px;" src="<?= $item['image'] ?>" alt="">
                                    </div>
                                    <!-- Product Description -->
                                    <div class="product-description">
                                        <h4 class="product-price"><?= $item['price']?></h4>
                                        <p><?= $item['title']?></p>
                                        <small><?= $item['category']?></small>
                                        <!-- Add to Cart -->
                                        <form class="" action="/checkout" method="post">
                                            <input type="text" name="productId" value="<?= $item['id']?>" hidden>
                                            <input type="submit" name="submitBnt" value="ADD TO CART" class="btn btn-primary add-to-cart-btn btn-checkout">
                                        </form>
                                    </div>
                                </div>
                              <?php endwhile ?>
                              <?php if ($products->num_rows < 1): ?>
                                <?php echo '<h2>Sorry Film not found</h2>' ?>
                              <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php include '../part/bottom.php'; ?>
