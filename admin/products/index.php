<?php
include '../part/top.php';
include('../autoload.php');
$model = new Model();
$products = $model->getAllProducts();
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Movies</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                          <tr>
                              <td>No</td>
                              <td>Title</td>
                              <td>Category</td>
                              <td>Price</td>
                              <td>Year</td>
                              <td>Rating</td>
                              <td>Time</td>
                              <td>Action</td>
                          </tr>
                        </thead>

                        <tbody>
                          <?php $n=1; while($item = $products->fetch_assoc()): ?>
                          <tr>
                              <td><?= $n++ ?></td>
                              <td><?= $item['title'] ?></td>
                              <td><?= $item['category'] ?></td>
                              <td><?= $item['price'] ?></td>
                              <td><?= $item['year'] ?></td>
                              <td><?= $item['rating'] ?></td>
                              <td><?= date('d-M-Y',strtotime( $item['date'])) . " " . date('H:i',strtotime( $item['time']))?></td>
                              <td>
                                  <a href="/admin/products/edit.php?id=<?= $item['id'] ?>">Update</a>
                                  <a href="/admin/products/delete.php?id=<?= $item['id'] ?>">Delete</a>
                              </td>
                          </tr>
                          <?php endwhile ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->
<?php include '../part/bottom.php'; ?>
