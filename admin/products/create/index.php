<?php
include '../../part/top.php';
include '../../autoload.php';
$model = new Model();
$categories = $model->getAllCategory();
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Movie</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <form role="form" method="post" action="" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input name="title" class="form-control">
                                    <p class="help-block">Enter title the movie. Ex : Avatar</p>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input name="price" class="form-control">
                                    <p class="help-block">Enter price the movie. Ex : 100</p>
                                </div>
                                <div class="form-group">
                                    <label>Rating</label>
                                    <input name="rating" type="number" step="0.1" class="form-control" min="0" max="10" maxlength="2">
                                    <p class="help-block">Enter rating the movie. Value 0 to 10</p>
                                </div>
                                <div class="form-group">
                                    <label>Date</label>
                                    <input name="date" class="form-control" type="text" value="<?= date('d-m-Y')?>" id="datepicker">
                                    <p class="help-block">Enter show date movie in cinema</p>
                                </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Year</label>
                                    <input name="year" class="form-control" maxlength="4">
                                    <p class="help-block">Enter release year the movie. Ex : 2010</p>
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" class="form-control">
                                        <option>- Select -</option>
                                        <?php while($item = $categories->fetch_assoc()):?>
                                        <option value="<?= $item['id']?>"><?= $item['name']?></option>
                                        <?php endwhile ?>
                                    </select>
                                    <p class="help-block">Enter category movie</p>
                                </div>
                                <div class="form-group">
                                    <label>Time</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input name="time" id="timepicker" type="text" class="form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                    <p class="help-block">Enter show time movie in cinema</p>
                                </div>
                                <div class="form-group">
                                    <label>File input</label>
                                    <input type="file" name="image">
                                </div>

                        </div>
                        <br><br>

                        <!-- /.col-lg-6 (nested) -->

                    </div>
                    <!-- /.row (nested) -->
                    <button type="submit" class="btn btn-primary" name="btnSubmit">Save</button>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

<!-- /#page-wrapper -->
<?php
include '../../part/bottom.php';

if (isset($_POST['btnSubmit'])) {
    $title = $_POST['title'];
    $year = $_POST['year'];
    $rating = $_POST['rating'];
    $price = $_POST['price'];
    $category = $_POST['category'];

    $date =date('Y-m-d', strtotime( $_POST['date']));
    $time = $_POST['time'];

    $addProduct = $model->addProduct($title, $year, $rating, $category, $date, $time, $price);
    if ($addProduct == false){
        echo "<script>alert('Failed input data')</script>";
    }else{
        echo "<script>alert('Success input data')</script>";
        //Uplaod Image
        $target_dir = "../../../assets/img/uploads/";
        $name = $_FILES["image"]["name"];
        $ext = end((explode(".", $name)));
        $target_file = $target_dir . $addProduct . "." . $ext;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            unlink($target_file);
            $uploadOk = 1;
        }
        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            // echo "Sorry, your file is too large.";
            echo "<script>alert('Sorry, your file is too large.')</script>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "<script>alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.')</script>";
            // echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<script>alert('Sorry, your file was not uploaded.')</script>";
            // echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                $url = "/assets/img/uploads/".$addProduct . "." . $ext;
                if ($model->updateImage($addProduct, $url)) {
                  echo "<script>alert('Success upload image')</script>";
                }
                echo "<script>window.location.href = '/admin/products';</script>";
            } else {
              echo "<script>alert('Error upload image')</script>";
            }
        }
    }
}

?>
